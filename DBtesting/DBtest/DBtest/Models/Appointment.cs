﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBtest.Models
{
    public class Appointment
    {
        [Key]
        public int Id { get; set; }
        public string PersonName { get; set; }
        public DateTime Date { get; set; }
    }
}
