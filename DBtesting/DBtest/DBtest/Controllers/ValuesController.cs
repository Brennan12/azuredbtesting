﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBtest.Data;
using DBtest.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DBtest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private TestDBContext _db;
        public ValuesController(TestDBContext db)
        {
            _db = db;        
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Appointment>> Get()
        {
            var appointments = _db.Appointments.ToList();


            return appointments;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public  ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post()
        {
            Appointment model = new Appointment();
            model.Date = DateTime.Now;
            model.PersonName = "Brennan";
            _db.Appointments.Add(model);
            _db.SaveChanges();
           
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
